<?php 
  $req_uri = $_SERVER['REQUEST_URI'];
  $req_uri = ereg_replace (',.*$', '', $req_uri);
  $uri = "http://" . ($_SERVER['HTTP_HOST'] ? $_SERVER['HTTP_HOST'] : 'suika.fam.cx') . $req_uri;
  $encoded_uri = urlencode ($uri);

  $ereq_uri = htmlspecialchars ($req_uri);
  $euri = htmlspecialchars ($uri);
  $eencoded_uri = htmlspecialchars ($encoded_uri);

  header('Content-Type: text/html; charset=utf-8');
?>
<!DOCTYPE HTML>
<html lang=ja>
<title>&lt;<?php echo $euri?>&gt; について</title>
<meta name=robots content="noindex,nofollow">
<link rev="made" href="mailto:webmaster@suika.fam.cx" />
<link rel="copyright" href="/c/gnu/fdl" />
<link rel="stylesheet" href="/www/style/html/xhtml" media="all" />
</head>
<body>
<h1><code class="uri">&lt;<?php echo $euri?>&gt;</code> について</h1>

<div class="section" id="INFO">
<h2>この頁についての情報</h2>

<ul>
<li><a href="<?php echo $ereq_uri?>,headers">鯖が送る 
<abbr lang="en" xml:lang="en" title="Hypertext Transfer Protocol">HTTP</abbr>
頭</a> <span class="weak">(<a href="http://cgi.w3.org/cgi-bin/headers" lang="en" xml:lang="en"><abbr
lang="en" xml:lang="en" title="World Wide Web Consortium">W3C</abbr>
<abbr lang="en" xml:lang="en" title="Hypertext Transfer Protocol">HTTP</abbr>
Head</a> を使用)</span></li>
<li><a href="http://web-sniffer.net/?verbose=on&sp=1&url=<?php echo $eencoded_uri?>">鯖が送る 
<abbr lang="en" xml:lang="en" title="Hypertext Transfer Protocol">HTTP</abbr>
頭</a> <span class="weak">(<a href="http://web-sniffer.net/" lang="en" xml:lang="en">Web‐Sniffer</a>
を使用)</span></li>
<li><a href="http://www.ircache.net/cgi-bin/cacheability.py?descend=on&amp;query=<?php echo $eencoded_uri?>">キャッシュ可能性</a>
<span class="weak">(<a lang="en" xml:lang="en" 
href="http://www.mnot.net/cacheability/">Cacheability Engine</a> を使用)</span></li>
<li><a href="<?php echo $ereq_uri?>,tools"><code class="uri">&lt;<?php echo $euri?>&gt;</code>
について</a></li>
</ul>
</div>

<div id="ALT" class="section">
<h2>他の版</h2>

<ul>
<li><a href="<?php echo $ereq_uri?>,text">平文版</a>
<span class="weak">(<a href="http://cgi.w3.org/cgi-bin/html2txt" lang="en" xml:lang="en"><abbr
lang="en" xml:lang="en" title="World Wide Web Consortium">W3C</abbr> html2txt</a>
を使用)</span></li>
<li><a href="<?php echo $ereq_uri?>,cvslog">版管理情報</a></li>
</ul>
</div>

<div class="section" id="VALIDATE">
<h2>検証</h2>

<ul>
<li><a href="<?php echo $ereq_uri?>,validate"><abbr
lang="en" xml:lang="en" title="Hypertext Markup Language">HTML</abbr> / 
<abbr lang="en" xml:lang="en" title="Standard Generalized Markup Language"
>SGML</abbr>
妥当性検証</a>
<span class="weak">(<a href="http://validator.w3.org/" lang="en" xml:lang="en"><abbr
lang="en" xml:lang="en" title="World Wide Web Consortium">W3C</abbr> <abbr
lang="en" xml:lang="en" title="Hypertext Markup Language">HTML</abbr> 
Validator</a> を使用)</span></li>
<li><a href="<?php echo $ereq_uri?>,lint"><abbr
lang="en" xml:lang="en" title="Hypertext Markup Language">HTML</abbr>
検証</a>
<span class="weak">(<a href="http://openlab.ring.gr.jp/k16/htmllint/" lang="en" xml:lang="en"
>Another <abbr lang="en" xml:lang="en" title="Hypertext Markup Language"
>HTML</abbr>‐Lint</a> を使用)</span></li>
<li><a href="<?php echo $ereq_uri?>,cssvalidate"><abbr
lang="en" xml:lang="en" title="Cascading Style Sheets">CSS</abbr> 検証</a>
<span class="weak">(<a href="http://jigsaw.w3.org/css-validator/" lang="en" xml:lang="en"><abbr
title="World Wide Web Consortium">W3C</abbr> <abbr
lang="en" xml:lang="en" title="Cascading Style Sheets">CSS</abbr>
Validator</a> を使用)</span></li>
<li><a href="<?php echo $ereq_uri?>,spell">綴りを検証</a>
<span class="weak">(<a href="http://www.w3.org/2002/01/spellchecker" lang="en" xml:lang="en"><abbr
title="World Wide Web Consortium">W3C</abbr> Spellchecker</a> を使用)</span></li>
<li><a href="<?php echo $ereq_uri?>,checklink">壊れたリンクがないか検証</a>
<span class="weak">(<a href="http://validator.w3.org/checklink" lang="en" xml:lang="en"><abbr
title="World Wide Web Consortium">W3C</abbr> Link Checker</a> を使用)</span></li>
<li><a href="<?php echo $ereq_uri?>,rchecklink">この頁とこの頁からリンクしている頁に壊れたリンクがないか検証</a>
<span class="weak">(<a href="http://validator.w3.org/checklink" lang="en" xml:lang="en"><abbr
title="World Wide Web Consortium">W3C</abbr> Link Checker</a> を使用)</span></li>
</ul>
</div>

<div class="section" id="NEARBY">
<h2>関連する資源</h2>

<ul>
<li><a href="<?php echo $ereq_uri?>,imglist">この階層の画像一覧</a>
(<a href="<?php echo $euri?>,imglist-detail">詳細情報付き</a>)</li>
<li><a href="./,tools">この階層について</a></li>
<li><a href="../,tools">一つ上の階層について</a></li>
</ul>
</div>

<div class="section" id="MISC">
<h2>その他</h2>

<ul>
  <li><a href="<?php echo $ereq_uri?>,m3u">このページの m3u ファイル</a>
</ul>
</body>
</html>
<? /*

LICENSE

Copyright 2005-2011 Wakaba <w@suika.fam.cx>.

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

*/ ?>
