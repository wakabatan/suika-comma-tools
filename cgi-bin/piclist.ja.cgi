#!/usr/bin/perl
use strict;
#use warnings;
use CGI::Carp qw(fatalsToBrowser);
use Path::Class;
use Encode;

sub error ($);
my %Opt;

for (split /[&;]/, $ENV{QUERY_STRING} || '') {
  my ($name, $val) = split /=/, $_, 2;
  $Opt{$name} = defined $val ? $val : 1;
  $Opt{$name} =~ s/%([0-9A-Fa-f]{2})/pack 'C', hex $1/ge;
}

my $dirpath = $Opt{path} || '/';
$dirpath =~ s{[^/]+\z}{};
my $dir;
if ($dirpath =~ m{^/~(wakaba|hero)/(.*)$}) {
  $dir = dir ('/home', $1, 'public_html')->subdir ($2)->cleanup;
  unless ($dir =~ m{^/home/(?:wakaba|hero)/public_html(?:/|$)}) {
    error 404;
  }
  error 404 unless -d $dir;
  $dir = $dir->resolve;
  unless ($dir =~ m{^/home/(?:wakaba|hero)/public_html(?:/|$)}) {
    error 404;
  }
  error 404 unless -d $dir;
} else {
  $dir = dir ('/home/httpd/html')->subdir ($dirpath)->cleanup;
  unless ($dir =~ m{^/home/httpd/html(?:/|$)}) {
    error 404; 
 }
  error 404 unless -d $dir;
  $dir = $dir->resolve;
  unless ($dir =~ m{^/home/httpd/html(?:/|$)}) {
    error 404;
  }
  error 404 unless -d $dir;
}

sub escape ($) {
  my $s = shift;
  $s =~ s/&/&amp;/g;
  $s =~ s/</&lt;/g;
  $s =~ s/>/&gt;/g;
  $s =~ s/"/&quot;/g;
  $s =~ s/'/&#x27;/g;
  $s;
}

sub rfc3339date ($) {
  my @gt = gmtime shift;
  sprintf '%04d-%02d-%02dT%02d:%02d:%02dZ',
          $gt[5] + 1900, $gt[4] + 1, @gt[3, 2, 1, 0];
}

sub filesize ($) {
  my $size = 0 + shift;
  if ($size > 2048) {
    $size /= 1024;
    if ($size > 2048) {
      $size /= 1024;
      sprintf '%.1f メガオクテット', $size;
    } else {
      sprintf '%.1f キロオクテット', $size;
    }
  } else {
    $size . ' オクテット';
  }
}

opendir DIR, $dir->stringify or error 403;
  my @all_files = sort grep {$_ and not /^\./ and /^[A-Za-z0-9._-]+$/}
                  (readdir DIR)[0..300];
close DIR;
my @files = grep {/\.(?:jpe?g|png|ico|gif|mng|xbm|JPE?G)(?:\.gz)?$/} @all_files;
my @dirs = grep {$_ ne 'CVS' and -d $dir->subdir ($_)} @all_files;

my %all_files = map { $_ => 1 } @all_files;

sub has_file ($) {
  my $name = shift;
  my $namelen = 1 + length $name;
  for (@all_files) {
    if ($name.'.' eq substr $_, 0, $namelen) {
      return 1;
    }
  }
  return 0;
}

sub preview_uri ($) {
  my $original_file_name = shift;
  $original_file_name =~ s/\..*$//;
  my $file_name = $original_file_name;
  if ($file_name =~ /-small$/) {
    return $file_name;
  } else {
    $file_name =~ s/-large$//;
    if (has_file $file_name . '-small') {
      return $file_name . '-small';
    } elsif (has_file $file_name) {
      return $file_name;
    } else {
      return $original_file_name;
    }
  }
}

my $title = '画像一覧';

print STDOUT "Content-Type: text/html; charset=euc-jp\n\n";

my $linkelement = '<link rel="stylesheet" href="/s/image-list" media="all" />';

$| = '';
print <<EOH;
<!DOCTYPE html SYSTEM>
<html lang="ja">
<head>
<base href="http://suika.fam.cx@{[escape $dirpath]}" />
<meta name="robots" content="noindex,nofollow">
<title>@{[$Opt{cframe} ? '' : (escape $dirpath) . qq< の>]}${title}</title>
${linkelement}
</head>
EOH

my $LISTq = q<>;
$LISTq .= q<;detail> if $Opt{detail};
$LISTq = substr $LISTq, 1;
$LISTq = ',imglist' . ($LISTq ? '?' . $LISTq : '');

if ($Opt{cframe}) {
  my $LISTqt = ($LISTq eq ',imglist' ? $LISTq . '?' : $LISTq . ';') . 'target=view';
  print qq{<frameset cols="25%,*">
             <frame src="$LISTqt" name="list" />
             <frame src="./" name="view" />
             <noframes>};
}

my $viewtarget = '';
my $listtarget = '';
my $parenttarget = '';
if ($Opt{target} =~ /^([a-z]+)$/) {
  $viewtarget = qq{ target="$1"};
  $listtarget = q{ target="_self"};
  $LISTq .= $LISTq eq ',imglist' ? qq{?target=$1} : qq{;target=$1};
  $parenttarget = q{ target="_parent"};
}

if ($Opt{detail}) {
  print qq{<body@{[$Opt{target}?' class="has-target"':'']}>
           <h1>${title}</h1>
           <div class="pictures detail">};
  
  for my $file_name (@files) {
    my $efile = escape $file_name;
    my $preview_uri = escape preview_uri $file_name;
    my $uri = $efile;
    $uri =~ s/\..+//g;
    my $id = uc $uri;
    my @cls = split /\./, lc $file_name;
    shift @cls;
    print qq{<div class="image-with-desc" id="FILE--$id">};
      print qq{<a href="$uri"$viewtarget>};
      print qq{<img src="$preview_uri" alt="" class="@{[join ' ', @cls, 's']}" /></a>};
      print qq{<dl><dt>URI</dt><dd>};
      print qq{<code class="uri">&lt;<a href="$uri"$viewtarget>$uri</a>&gt;</code></dd>};
      print qq{<dt>ファイル名</dt><dd>};
      print qq{<code class="file"><a href="$efile"$viewtarget>$efile</a></code></dd>};
      print qq{<dt>日付</dt><dd>};
      print rfc3339date ([stat $dir->file ($file_name)]->[9]);
      print qq{<dt>大きさ</dt><dd>};
      print filesize ([stat $dir->file ($file_name)]->[7]);
      print qq{</dd>};
      print qq{</dl>};
    print q{</div>};
  }

  my @videos = grep {/\.(?:avi|mpe?g|mp3|wav|mid|swf)(?:\.gz)?$/i} @all_files;
  for my $file_name (@videos) {
    my $efile = escape $file_name;
    my $uri = $efile;
    $uri =~ s/\..+//g;
    print q{<div class="image-with-desc">};
      print qq{<a href="$uri"$viewtarget><img src="/~wakaba/archive/2005/movie-1" alt="" /></a>};
      print qq{<dl><dt>URI</dt><dd>};
      print qq{<code class="uri">&lt;<a href="$uri"$viewtarget>$uri</a>&gt;</code></dd>};
      print qq{<dt>ファイル名</dt><dd>};
      print qq{<code class="file"><a href="$efile"$viewtarget>$efile</a></code></dd>};
      print qq{<dt>日付</dt><dd>};
      print rfc3339date ([stat $dir->file ($file_name)]->[9]);
      print qq{</dd>};
      print qq{<dt>大きさ</dt><dd>};
      print filesize ([stat $dir->file ($file_name)]->[7]);
      print qq{</dd>};
      print qq{</dl>};
    print q{</div>};
  }

  for my $dir_name (@dirs) {
    my $edir = escape $dir_name;
    print q{<div class="dir dir-with-desc">};
      if (-f $dir . '/' . $dir_name . '/favicon.png' or
          -f $dir . '/' . $dir_name . '/favicon.ico') {
        print qq{<img src="$edir/favicon" alt="" class="mini-icon" />};
      } else {
        print qq{<img src="/icons/folder" alt="" class="mini-icon" />};
      }
      print qq{<code class="file"><a href="$edir/$LISTq"$listtarget>$edir/</a></code>};
    print q{</div>};
  }

  for (['cover', '表紙', 'start'],
       ['introduction', 'はじめに', 'start'],
       ['intro', 'はじめに', 'start'],
       ['README', 'はじめに'],
       ['contents', '目次', 'contents'],
       ['list', '一覧', 'contents'],
       ['description', '説明'],
       ['index', '索引', 'index'],
       ['latest', '最新版'],
       ['current', '現行版']) {
    if (has_file $_->[0]) {
      print q{<div class="file file-with-desc">};
        print q{<img src="/icons/layout" alt="" class="mini-icon" />};
        print qq{<a href="$_->[0]" rel="$_->[2]"$viewtarget>$_->[1]</a>};
      print q{</div>};
    }
  }

  print q{<div class="dir-up dir-with-desc">};
    print q{<img src="/icons/forward" alt="" class="mini-icon" />};
    print qq{<a href="../$LISTq" rel="up"$listtarget>上の階層</a>};
  print q{</div>};
  
  print q{</div>};

} else {  ## Normal Listing Mode
  print qq{<body@{[$Opt{target}?' class="has-target"':'']}>
           <h1>${title}</h1>
           <div class="pictures">};
  my $imgsattr = ' class="s"';

  for my $file_name (@files) {
    my $uri = escape $file_name;
    $uri =~ s/\..+$//g;
    my $preview_uri = escape preview_uri $file_name;
    print '<a href="'.$uri.'"'.$viewtarget.'>';
    print '<img src="'.$preview_uri.'" alt="'.$uri.'"'.$imgsattr.' />';
    print "</a>\n";
  }

  print q{</div>};

  print q{<ul>};
  for my $dir_name (@dirs) {
    my $edir = escape $dir_name;
    print q{<li class="dir">};
      print qq{<code class="file"><a href="$edir/$LISTq"$listtarget>$edir/</a></code>};
    print q{</li>};
  }
  print q{</ul>};
  print q{<div class="dir-up dir-with-desc">};
    print q{<img src="/icons/forward" alt="" class="mini-icon" />};
    print qq{<a href="../$LISTq" rel="up"$listtarget>上の階層</a>};
  print q{</div>};
}

my $cvslink = '';
if (-d $dir->subdir ('CVS')) {
  if (-f $dir->file ('CVS', 'Root')) {
    if (open my $root, '<', $dir->file ('CVS', 'Root')) {
      if (<$root> =~ m#^(/home/cvs|/home/wakaba/pub/cvs)/?$#) {
        my $rpath = $1;
        if (-f $dir->file ('CVS', 'Repository')) {
          if (open my $repo, '<', $dir->file ('CVS', 'Repository')) {
            my $reppath = escape <$repo>;
            $reppath =~ tr/\x0A\x0D//d;
            if ($reppath) {
              $cvslink = qq{ <a href="/gate/cvs/$reppath/@{[
                           {q[/home/cvs] => '',
                            q[/home/wakaba/pub/cvs] => '?cvsroot=Wakaba'}->{$rpath}
                         ]}" rel="history"$parenttarget>この階層の履歴</a>};
            }
          }
        }
      }
    }
  }
}

for (
  ['README', 'お読みください'],
  ['README.en', 'お読みください'],
  ['README.ja', 'お読みください'],
  ['LICENSE', 'ライセンス'],
  ['ChangeLog', '変更履歴'],
) {
  my ($file_name, $title) = @$_;
  if ($all_files{$file_name}) {
    my $f = $dir->file ($file_name);
    if ($Opt{target}) {
      print qq{<p><a href="$file_name"$viewtarget>$title</a>};
    } else {
      my $content = $f->slurp;
      if ($content =~ /\x1B/) {
        $content = decode 'iso-2022-jp', $content;
        $content = encode 'euc-jp', $content;
      }
      print qq{<div class=section id="$file_name">};
      print qq{<h2><a href="$file_name"$viewtarget>$title</a></h2><pre>};
      my $econtent = escape $content;
      $econtent =~ s{^([0-9A-Za-z_.-]+)$}{
        if ($all_files{$1}) {
          qq{<code class=file><a href="$1">$1</a></code>};
        } else {
          $1;
        }
      }gem;
      $econtent =~ s{&lt;(https?://[\x21-\x7E]+?)&gt;}{
        qq{<code class=uri>&lt;<a href="$1">$1</a>&gt;</code>};
      }ge;
      print $econtent;
      print qq{</pre></div>};
    }
  }
}

print <<EOH;

<div class="footer">
<div class="navigation">
[<a href="/" rel="home"$parenttarget>/</a>]
[<a href="." rel="contents"$parenttarget>この階層</a>$cvslink]
[画像一覧 (<a href=",imglist" rel="alternate"$parenttarget>簡易</a>, 
<a href=",imglist?cframe" rel="alternate"$parenttarget>簡易・横分割</a>,
<a href=",imglist?detail" rel="alternate"$parenttarget>詳細</a>,
<a href=",imglist?detail;cframe" rel="alternate"$parenttarget>詳細・横分割</a>)]
</div>
</div>
</body>
EOH

print q{</noframes></frameset>} if $Opt{cframe};

print q{</html>};

sub error ($) {
  my $code = shift;
  my $phrase = {
    403 => 'Forbidden',
    404 => 'Not found',
  }->{$code};
  print "Status: $code $phrase\nContent-Type: text/plain; charset=us-ascii\n\n";
  print "$code $phrase";
  exit;
} # error

1;

__END__



=head1 CHANGES

2005-02-26  Wakaba <w@suika.fam.cx>

        - Frame mode implemented.

2005-02-25  Wakaba <w@suika.fam.cx>

        - Use external style sheet. 
        - Detail mode implemented.

2001-06-25  Wakaba <wakaba@61.201.226.127>

	- In default, images are sized by stylesheet.  When ?realsize=1,
	  images are not specified its size.
	- Images are linked to itself.

2001-05-17  Wakaba

	- New File.

=head1 LICENSE

Public Domain. 

=cut

# $Date: 2007/11/22 12:50:09 $
